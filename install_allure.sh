#!/bin/bash

set -e 

sudo docker run 	-p 5050:5050 \
			--name allure \
			--user="1001:1001" \
			--restart=always \
			-e CHECK_RESULTS_EVERY_SECONDS=3 -e KEEP_HISTORY=1 \
			-v /data/allure-results:/app/allure-results \
  			-v /data/allure-results:/app/default-reports \
			-d frankescobar/allure-docker-service
