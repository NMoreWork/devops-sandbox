#!/bin/bash

set +e

# Удаляем старые версии Docker и связанные пакеты
sudo apt-get remove -y docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc

# Устанавливаем необходимые пакеты
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common

# Добавляем GPG ключ Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Добавляем репозиторий Docker в источники APT
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Обновляем индексы пакетов и устанавливаем Docker
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# Запускаем Docker сервис
sudo systemctl start docker
sudo systemctl enable docker

# Проверяем установку, запускаем контейнер hello-world
sudo docker run hello-world

# Даем доступ не root пользователям: WARNING!!!
# Добавляем текущего пользователя в группу docker
sudo usermod -aG docker $USER

# Перезапускаем Docker сервис
sudo systemctl restart docker

# Даем права на чтение и запись для сокета Docker
sudo chmod 666 /var/run/docker.sock

# Проверяем, что Docker images работает без root:
docker images
echo ""
echo -e "\e[1;31mEverything is OK. You can now work with Docker without sudo\e[0m"
