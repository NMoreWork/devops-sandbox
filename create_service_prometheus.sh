#!/bin/bash
set -e 

sudo useradd \
	    --system \
	    --no-create-home \
	    --shell /bin/false prometheus

wget https://github.com/prometheus/prometheus/releases/download/v2.45.2/prometheus-2.45.2.linux-amd64.tar.gz

tar -xvf prometheus-2.45.2.linux-amd64.tar.gz

sudo mkdir -p /data /etc/prometheus

cd prometheus-2.45.2.linux-amd64
sudo mv prometheus promtool /usr/local/bin/
sudo mv consoles/ console_libraries/ /etc/prometheus/
sudo mv prometheus.yml /etc/prometheus/prometheus.yml
sudo chown -R prometheus:prometheus /etc/prometheus/ /data/
cd
rm -rf prometheus*
prometheus --version
sudo vim /etc/systemd/system/prometheus.service
sudo systemctl enable prometheus
sudo systemctl start prometheus
sudo systemctl status prometheus
journalctl -u prometheus -f --no-pager

